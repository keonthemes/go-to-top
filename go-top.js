( function( $ ){

    function scrollToTop ( param ){

        this.markup   = null,
        this.selector = null;
        this.fixed    = true;
        this.visible  = false;

        this.init = function(){
            if( this.valid() ){

                if( typeof param != 'undefined' && typeof param.fixed != 'undefined' ){
                    this.fixed = param.fixed;
                }

                this.selector = ( param && param.selector ) ? param.selector : '#go-top';

                this.getMarkup();
                var that = this;

                jQuery( 'body' ).append( this.markup );

                if( this.fixed ){

                    jQuery( this.selector ).hide();

                    var windowHeight = jQuery( window ).height();

                    jQuery( window ).scroll(function(){

                        var scrollPos     = jQuery( window ).scrollTop();

                        if(  ( scrollPos > ( windowHeight - 100 ) ) ){

                            if( false == that.visible ){
                                jQuery( that.selector ).addClass( 'show' );
                                that.visible = true;
                            }
                            
                        }else{

                            if( true == that.visible ){
                                jQuery( that.selector ).removeClass( 'show' );
                                that.visible = false;
                            }
                        }
                    });

                    this.bindEvent();
                }
            }
        }

        this.bindEvent = function(){
            jQuery( document ).on( 'click', this.selector, function(e){
                e.preventDefault();
                $("html, body").animate({ scrollTop: 0 }, 800);
            });
        }

        this.getMarkup = function(){

            var position = this.fixed ? 'fixed':'absolute';

            var wrapperStyle = 'style="position: '+position+'; z-index:999999; bottom: 20px; right: 20px;"';

            var buttonStyle  = 'style="cursor:pointer;display: inline-block;padding: 10px 20px;background: #f15151;color: #fff;border-radius: 2px;"';

            var markup = '<div ' + wrapperStyle + ' id="go-top"><span '+buttonStyle+'>Scroll To Top</span></div>';

            this.markup   = ( param && param.markup ) ? param.markup : markup;
        }

        this.valid = function(){

            if( param && param.markup && !param.selector ){
                alert( 'Please provide selector. eg. { markup: "<div id=\'scroll-top\'></div>", selector: "#scroll-top"}' );
                return false;
            }

            return true;
        }
    };

    jQuery( document ).ready( function (e) {
       // Initializing scroll top js
       new scrollToTop({
        markup   : '<a href="#" class="" id="go-top"><span class="button-outline"><i class="fa fa-arrow-up"></i></span></a>',
        selector : '#go-top'
       }).init();
    });
	
})(jQuery)