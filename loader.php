<?php

class Back_To_Top{
	
	public function __construct(){
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
		add_action( 'wp-head', array( $this, 'style' ) );
	}

	public function style(){
		?>
		<style>

		</style>
		<?php
	}

	public function scripts(){
		
		wp_enqueue_script( 'go-top', get_theme_file_uri( '/modules/go-top/go-top.js' ), array( 'jquery' ) );
	}
}

new Back_To_Top();